$( function() {

	var flag = 0;

	$.ajax({

		type:"GET",
		url:"./get_data.php",
		data:{
			'offset':0,
			'limit':5
		},
		success: function(data){
			$("#container").append(data);
			flag += 5;
		}
	});

	$(window).scroll(function () {
		/* body... */
		console.log($(window).scrollTop());
		if ($(window).scrollTop() >= ($(document).height() - $(window).height()) ) {

			$.ajax({

				type:"GET",
				url:"./get_data.php",
				data:{
					'offset':flag,
					'limit':3
				},
				success: function(data){
					$("#container").append(data);
					flag += 3;
				}

			});
		}

	});


})();